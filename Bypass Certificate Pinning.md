# Bypass certificate pinning

In-app certificate pinning can be bypassed using the Objection utility (built on top of Frida).

## Requirements

1. Android Platform Tools.
2. A rooted Android device.
3. Frida
4. Objection

## Instructions

** N.B. Please ensure USAP has been disabled otherwise Frida will not work. This can be achieved from a root console on the handset by running `setprop persist.device_config.runtime_native.usap_pool_enabled false`. **

Setup a Python 3 virtual environment, and install Frida and Objection.

```bash
$ python3 -m venv venv
$ source ./venv/bin/activate
$ pip install frida-tools objection
```

Download [`frida-server`](https://github.com/frida/frida/releases) to your Android handset, ensuring that the architecture is correct. At the time of writing, the latest version for Android arm64 is `frida-server-14.2.18-android-arm64.xz`.

```bash
$ unxz frida-server-14.2.18-android-arm64.xz
$ mv frida-server-14.2.18-android-arm64 frida-server
$ adb push frida-server /sdcard/
$ adb root
$ adb shell "mv /sdcard/frida-server /data/local/tmp/frida-server"
$ adb shell "chmod 755 /data/local/tmp/frida-server"
```

Run `frida-server` on the handset in the background.

```bash
$ adb shell "/data/local/tmp/frida-server &"
```

Identify the package name for the app whose certificate pinning you will disable.

```bash
$ adb shell pm list packages
...
package:com.example.certpinning
...
```

Launch the application using Objection, and disable certificate pinning.

```bash
$ objection --gadget com.example.certpinning explore
Using USB device `JackPhone`
Agent injected and responds ok!

     _   _         _   _
 ___| |_|_|___ ___| |_|_|___ ___
| . | . | | -_|  _|  _| | . |   |
|___|___| |___|___|_| |_|___|_|_|
      |___|(object)inject(ion) v1.11.0

     Runtime Mobile Exploration
        by: @leonjza from @sensepost

[tab] for command suggestions
com.example.certpinning on (JackPhone: 11) [usb] # android sslpinning disable
(agent) Custom TrustManager ready, overriding SSLContext.init()
(agent) Found okhttp3.CertificatePinner, overriding CertificatePinner.check()
(agent) Found okhttp3.CertificatePinner, overriding CertificatePinner.check$okhttp()
(agent) Found com.android.org.conscrypt.TrustManagerImpl, overriding TrustManagerImpl.verifyChain()
(agent) Found com.android.org.conscrypt.TrustManagerImpl, overriding TrustManagerImpl.checkTrustedRecursive()
(agent) Registering job 586622. Type: android-sslpinning-disable
```
