# Rooting a OnePlus 6T

The easiest way to root a OnePlus 6T handset is to flash it with LineageOS. Recent versions of LineageOS allow you to enable/disable 'root' directly from the system's settings menu, making it ideal for reverse engineering applications which have root detection.

At the time of writing, the latest version of LineageOS is v18.1 (Android 11), and can be downloaded from [here](https://download.lineageos.org/fajita). The official LineageOS installation instructions can be found on its [wiki](https://wiki.lineageos.org/devices/fajita/install).

N.B. Some applications (annoyingly) require the Google apps suite in order to run. These can be downloaded from [here](https://wiki.lineageos.org/gapps.html).

## Requirements

1. Android Platform Tools.
2. An OEM unlocked bootloader.
3. LineageOS recovery image.
4. Partition copy utility.
5. LineageOS system image.
6. Google apps suite.

## Instructions

### Unlocking the OnePlus 6T bootloader

- Boot into the device's bootloader.
  - If ADB is enabled on the handset, this can be achieved using `adb reboot bootloader`.
  - Otherwise, with the device powered off, hold `Volume Up` + `Volume Down` + `Power`.
- Run `fastboot oem unlock`.
- Reboot the device.

### Flashing the LineageOS bootloader

- Boot into the device's bootloader.
  - If ADB is enabled on the handset, this can be achieved using `adb reboot bootloader`.
  - Otherwise, with the device powered off, hold `Volume Up` + `Volume Down` + `Power`.
- Flash the recovery image using `fastboot flash boot <recovery_filename>.img`.

### Safeguarding the partitions

- Download the parition copy utility from [here](https://www.androidfilehost.com/?fid=2188818919693768129).
- Sideload the `copy-partitions-20210323_1922.zip` package.
  - On the handset, select 'Apply Update', then 'Apply from ADB'.
  - Run `adb sideload copy-partitions-20210323_1922.zip`.
  - Accept the signature verification warning.
- Reboot to recovery by selecting 'Advanced', then 'Reboot to recovery'.

### Installing LineageOS

- Perform a factory reset.
  - On the handset, select 'Factory Reset', then 'Format data / factory reset'.
- Sideload the LineageOS package.
  - On the handset, select 'Apply Update', then 'Apply from ADB'.
  - Run `adb sideload <lineageos_image>.zip`.
  - If the process succeeds the output will stop at 47% and report adb: failed to read command.
- Reboot to recovery by selecting 'Advanced', then 'Reboot to recovery'.
- Sideload the Google apps package BEFORE BOOTING INTO THE OS.
  - On the handset, select 'Apply Update', then 'Apply from ADB'.
  - Run `adb sideload <google_apps>.zip`.
  - Accept the signature verification warning.
- Reboot!
